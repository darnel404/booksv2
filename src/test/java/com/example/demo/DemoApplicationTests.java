package com.example.demo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Test
    public void contextLoads() {
    }

    Book book = new Book();

    @Before
    public void init(){
        book.setId(1);
        book.setTitle("Test");
        book.setDetail("Test");
    }

    @Test
    public void testBook1(){
        assertTrue(!book.getId().equals(null));

    }
}
