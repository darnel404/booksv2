package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public List<Book> books() {
        return bookService.getAllBooks();
    }

    @GetMapping("/books/{id}")
    public Optional<Book> getBook(@PathVariable("id") Long id){
        return bookService.getBook(id);
    }

    @GetMapping("/books/total")
    public Response getTotal(){
        return bookService.getTotalBooks();
    }

    @PutMapping("/books")
    public Response addBook(@RequestBody Book book){
        return bookService.addBook(book);
    }

    @PostMapping("/books")
    public Response updateBook(@RequestBody Book book){
        return bookService.updateBook(book);
    }

    @DeleteMapping("/books/{id}")
    public Response deleteBook(@PathVariable("id") Long id){
        return bookService.deleteBook(id);
    }
}
