package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {


    Response response = new Response();

    @Autowired
    private BookDao bookDao;

    public List<Book> getAllBooks() {
        return bookDao.findAllByOrderByIdAsc();
    }

    public Optional<Book> getBook(Long id){
        return bookDao.findById(id);
    }

    public Response getTotalBooks(){
        response.setResponse(String.valueOf(bookDao.count()));
        return  response;
    }

    public Response addBook(Book book){
        bookDao.save(book);
        response.setResponse("Book Added Successfully");
        return response;
    }

    public Response updateBook(Book book){
        bookDao.save(book);
        response.setResponse("Book Updated Successfully");
        return response;
    }


    public Response deleteBook(Long id){
        bookDao.deleteById(id);
        return response;
    }

}
